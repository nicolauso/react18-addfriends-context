import PropTypes from "prop-types";
import { UsersContext } from "../contexts/UsersContext";
import { useContext } from "react";
import { Link } from "react-router-dom";

function User() {
  const { users, friends, setFriends } = useContext(UsersContext);

  const handleCurrentUserToAddFriend = (user) => {
    if (friends.includes(user)) {
      setFriends(friends.filter((friend) => friend.username !== user.username));
    } else {
      setFriends([...friends, user]);
    }
  };
  console.log(friends);

  return (
    <div>
      <h1>Users catalog</h1>
      <Link to="/friendsList" className="button">See my friends list</Link>
      {users.map((user, id) => (
        <div className="User" key={id}>
          <h3>{user.username}</h3>
          <p>Name: {user.name}</p>
          <p>Email: {user.email}</p>
          <button onClick={() => handleCurrentUserToAddFriend(user)}>
            {friends.some(friend => friend.username === user.username) ? "Unfollow" : "Follow"}
          </button>
          <hr />
        </div>
      ))}
    </div>
  );
}

// User.propTypes = {
//   username: PropTypes.string.isRequired,
//   name: PropTypes.string.isRequired,
//   email: PropTypes.string.isRequired,
//   friends: PropTypes.array.isRequired,
//   setFriends: PropTypes.func.isRequired,
// };

export default User;
