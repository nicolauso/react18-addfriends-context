//import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { UsersContext } from "../contexts/UsersContext";
import { useContext } from "react";

function FriendList() {

  const { friends } = useContext(UsersContext);

  return (
    <div className="FriendList">
      <h1>My friend&apos;s list</h1>
      <Link to="/" className="button">Back to users</Link>
      <h2>{friends.length < 2 ? "Your friend" : "Your friends"}</h2>
      {friends.length
        ? friends.map((friend) => <p key={friend.id}>{friend.username}</p>)
        : "Add your first friend below"}
      <hr />
       
    </div>
  );
}

// FriendList.propTypes = {
//   friends: PropTypes.array.isRequired,
// };

export default FriendList;
