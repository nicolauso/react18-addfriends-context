import { createContext, useState, useEffect } from "react";
import axios from "axios";

const UsersContext = createContext();

function UserProvider({ children }) {
  const [users, setUsers] = useState([]);
  const [friends, setFriends] = useState([
    { id: 0, username: "Alice", email: "alice@gmail.com" },
    { id: 1, username: "Patrizia", email: "patrizia@gmail.com" },
  ]);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        setUsers(response.data);
      })
      .catch((error) => {
        console.error("Error fetching users:", error);
      });
  }, []);

  const usersContextValue = {
    users,
    setUsers,
    friends,
    setFriends,
  };

  return (
    <>
      <UsersContext.Provider value={usersContextValue}>
        {children}
      </UsersContext.Provider>
    </>
  );
}

export { UsersContext, UserProvider };
