import "./App.css";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import FriendList from "./components/FriendList";
import User from "./components/User";
import { UserProvider } from "./contexts/UsersContext";

function App() {
  return (
    <div className="App">
      <UserProvider>
        <Routes>
          <Route path="/" element={<User />} />
          <Route path="/friendsList" element={<FriendList />} />
        </Routes>
      </UserProvider>
    </div>
  );
}

export default App;
